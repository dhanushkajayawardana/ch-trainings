<?php

//trainings related routes for backend
Route::group(['name' => 'trainings', 'groupName' => 'trainings'], function () {
    Route::get('/trainings', 'PluginsControllers\TrainingsController@index')->name('trainings');
    Route::get('/trainings/create', 'PluginsControllers\TrainingsController@create')->name('trainings.create');
    Route::post('/trainings/create', 'PluginsControllers\TrainingsController@store')->name('trainings.store');

    Route::get('/trainings/edit/{id}', 'PluginsControllers\TrainingsController@edit')->name('trainings.edit');
    Route::put('/trainings/update/{id}', 'PluginsControllers\TrainingsController@update')->name('trainings.update');
    Route::delete('/trainings/delete/{id}',
        'PluginsControllers\TrainingsController@destroy')->name('trainings.delete');

    Route::get('/get-ajax-trainings',
        'PluginsControllers\TrainingsController@searchItem')->name('ajaxSearchTrainings');
});


//training instructor related
Route::group(['name' => 'instructor', 'groupName' => 'instructor'], function () {
    Route::get('/instructors/grid', 'PluginsControllers\InstructorsController@grid')->name('instructorGrid');
    Route::resource('/instructors', 'PluginsControllers\InstructorsController');
});
