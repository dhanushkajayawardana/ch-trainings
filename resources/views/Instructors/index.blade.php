@extends('Admin.layout')

@section('content')

    <div class="content-header row">
        <div class="content-header-left col-md-6 col-12 mb-2">
            <div class="row breadcrumbs-top">
                <div class="breadcrumb-wrapper col-12">
                    @include('Admin.partials.breadcumbs',['header'=>__('general.Instructors')])
                </div>
            </div>
        </div>

        <div class="content-header-right col-md-6 col-12 mb-2">
            <div class="mb-1 pull-right">
                <a href="{{route('admin.instructors.create')}}"
                   class="btn btn-secondary btn-block-sm"><i
                            class="ft-file-plus"></i> {{__('instructors.Create Instructor')}}</a>

            </div>
        </div>
    </div>
    @include('Admin.partials.form-alert')

    <div class="content-body">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-content show">
                        <div class="card-body">
                            <div class="table-responsive">
                                <table class="table table-bordered table-striped table-" id="thegrid">
                                    <thead>
                                    <tr>
                                        <th>{{__('instructors.Id')}}</th>
                                        <th>{{__('instructors.User Id')}}</th>
                                        <th>{{__('instructors.Email')}}</th>
                                        <th>{{__('instructors.First Name')}}</th>
                                        <th>{{__('instructors.Last Name')}}</th>
                                        <th>{{__('instructors.Avatar')}}</th>
                                        <th>{{__('instructors.Rank')}}</th>
                                        <th ></th>
                                        <th ></th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection



@section('scripts')

    <link rel="stylesheet" type="text/css" href="{{ asset("vendors/css/tables/datatable/datatables.min.css")}}">
    <script src="{{ asset("vendors/js/tables/datatable/datatables.min.js")}}"  type="text/javascript"></script>
    <script type="text/javascript">
        var theGrid = null;
        $(document).ready(function(){
            var editRoute = "{{route('admin.instructors.edit',['instructor'=>'sampleId'])}}";
            theGrid = $('#thegrid').DataTable({"bStateSave": true,
                "language": {
                    "url":"{{ asset(__('general.dataTable'))}}"
                },
                "processing": true,
                "serverSide": true,
                "ordering": true,
                "ajax": "{{route('admin.instructorGrid')}}",
                "columnDefs": [
                    {
                        "render": function ( data, type, row ) {
                            return '<a >'+data+'</a>';
                        },"className":"action-col","orderable": false,
                        "targets": 0
                    },
                    {
                        "render": function ( data, type, row ) {
                            return '<img src="'+data+'" class="width-100">';
                        },"className":"action-col","orderable": false,
                        "targets": 5
                    },
                    {
                        "render": function ( data, type, row ) {
                            return '<a href="'+editRoute.replace('sampleId',row[0])+'"  class="btn btn-info btn-sm">{{__('instructors.Update')}}</a>';
                        },"className":"action-col","orderable": false,
                        "targets": 7
                    },
                    {
                        "render": function ( data, type, row ) {
                            return '<a href="#" onclick="return doDelete('+row[0]+')" class="btn btn-danger btn-sm">{{__('instructors.Delete')}}</a>';
                        },"className":"action-col","orderable": false,
                        "targets": 7+1
                    },
                ]
            });
        });
        function doDelete(id) {
            // if(confirm('You really want to delete this record?')) {
            var deleteRoute = "{{route('admin.instructors.destroy',['instructor'=>'sampleId'])}}";
                swal({
                    title: "{{__('general.Warning!')}}",
                    text: "{{__('general.Are you sure you need to delete this item? this change cannot be undone.')}}",
                    icon: "warning",
                    buttons: {
                        cancel: {
                            text: "{{__('general.Cancel')}}",
                            value: null,
                            visible: !0,
                            className: "",
                            closeModal: !1
                        },
                        confirm: {
                            text: "{{__('general.Yes.Delete')}}",
                            value: !0,
                            visible: !0,
                            className: "",
                            closeModal: !1
                        }
                    }
                }).then(e => {
                    if (e) {
                        $.ajax({
                            dataType: 'json',
                            method: 'delete',
                            url: deleteRoute.replace('sampleId',id),
                        }).done(function (response) {
                            swal("{{__('general.Success!')}}", "{{__('general.Item deleted!!')}}", "success").then(() => {
                                theGrid.ajax.reload();
                            });
                        }).fail(function (erroErrorr) {
                            swal("{{__('general.Error')}}", "{{__('general.Error Occured')}}", "error");
                        });
                    } else {
                        swal("{{__('general.Cancelled')}}", "{{__("general.It's safe")}}", "error");
                    }
                });
               {{--$.ajax({ url: '{{ url('/instructors') }}/' + id, type: 'DELETE'}).success(function() {--}}
                {{--theGrid.ajax.reload();--}}
               {{--});--}}
            // }
            return false;
        }
    </script>
    <script src="{{ asset("vendors/js/extensions/sweetalert.min.js") }}" type="text/javascript"></script>
    <script>
        $('.delete-training').on('click', function (e) {
            e.preventDefault();
            var id = $(this).data('id');

            swal({
                title: "{{__('general.Warning!')}}",
                text: "Are you sure you need to delete this training? this change cannot be undone.",
                icon: "warning",
                buttons: {
                    cancel: {
                        text: "{{__('general.Cancel')}}",
                        value: null,
                        visible: !0,
                        className: "",
                        closeModal: !1
                    },
                    confirm: {
                        text: "{{__('general.Yes.Delete')}}",
                        value: !0,
                        visible: !0,
                        className: "",
                        closeModal: !1
                    }
                }
            }).then(e => {
                if (e) {
                    $.ajax({
                        dataType: 'json',
                        method: 'delete',
                        url: "/trainings/delete/" + id,
                    }).done(function (response) {
                        swal("{{__('general.Success!')}}", "{{__('general.Item deleted!!')}}", "success").then(() => {
                            location.reload();
                        });
                    }).fail(function (erroErrorr) {
                        swal("{{__('general.Error')}}", "{{__('general.Error Occured')}}", "error");
                    });
                } else {
                    swal("{{__('general.Cancelled')}}", "{{__("general.It's safe")}}", "error");
                }
            });

        });
    </script>
@endsection