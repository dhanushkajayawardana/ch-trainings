@extends('Admin.layout')

@section('content')
    <div class="content-header row">
        <div class="content-header-left col-md-6 col-12 mb-2">
            <div class="row breadcrumbs-top">
                <div class="breadcrumb-wrapper col-12">
                    @if (isset($model))
                        @include('Admin.partials.breadcumbs',['header' => $model->first_name,'params' => $model])
                    @else
                        @include('Admin.partials.breadcumbs',['header'=>__('general.Create Instructor')])
                    @endif
                </div>
            </div>
        </div>
    </div>
    @include('Admin.partials.form-alert')

    <div class="content-body">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        @if (isset($model))
                            <h4 class="card-title" id="basic-layout-form-center">{{__('instructors.Update instructor')}}</h4>
                        @else
                            <h4 class="card-title" id="basic-layout-form-center">{{__('instructors.Create new instructo')}}r</h4>
                        @endif
                    </div>
                    <div class="card-content show">
                        <div class="card-body">
                            @if(isset($model))
                            <form action="{{ route('admin.instructors.update',['instructor'=>$model->id]) }}" method="POST"
                                  class="form-horizontal" enctype="multipart/form-data">
                                @else
                                    <form action="{{route('admin.instructors.store')}}" method="POST"
                                          class="form-horizontal" enctype="multipart/form-data">
                                @endif
                                {{ csrf_field() }}

                                @if (isset($model))

                                    <input type="hidden" name="_method" value="PATCH">

                                    <div class="form-group">
                                        <div class="col-sm-12">
                                            <input type="hidden" name="id" id="id" class="form-control" readonly="readonly" value="{{$model->id}}">
                                        </div>
                                    </div>
                                @endif

                                <div class="form-group">
                                    <label for="first_name" class="col-sm-3 control-label">{{__('instructors.First Name')}}</label>
                                    <div class="col-sm-12">
                                        <input required type="text" name="first_name" id="first_name" class="form-control" value="{{isset($model) ? $model->first_name : (old('first_name'))?? '' }}">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="last_name" class="col-sm-3 control-label">{{__('instructors.Last Name')}}</label>
                                    <div class="col-sm-12">
                                        <input required type="text" name="last_name" id="last_name" class="form-control" value="{{isset($model) ? $model->last_name : '' }}">
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label for="first_name" class="col-sm-3 control-label">{{__('instructors.Email')}}</label>
                                    <div class="col-sm-12">
                                        <input required type="email" name="email" id="email" class="form-control" value="{{isset($model) ? $model->email : '' }}">
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label for="avatar" class="col-sm-3 control-label">{{__('instructors.Avatar')}}</label>
                                    <div class="col-sm-12">
                                        <input id="image" type="file" class="form-control"
                                               name="avatar">

                                        @if(isset($model))
                                            <input  type="hidden" class="form-control" name="featured_image_old" value="{{$model->avatar}}">
                                            <img id="image_preview" src="{{asset($model->avatar)}}" alt="featured" class="height-150 img-thumbnail">
                                        @else
                                            <img id="image_preview" class="hide height-150 img-thumbnail" >
                                        @endif
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="rank" class="col-sm-3 control-label">{{__('instructors.Rank')}}</label>
                                    <div class="col-sm-12">
                                        <input required type="text" name="rank" id="rank" class="form-control" value="{{isset($model) ? $model->rank : '' }}">
                                    </div>
                                </div>
                                {{--<div class="form-group">--}}
                                    {{--<label for="rating" class="col-sm-3 control-label">Rating</label>--}}

                                    {{--<div class="col-sm-12">--}}
                                        {{--<div id="default-star-rating" data-score="{{ isset($model) ? $model->rating : 0 }}" ></div>--}}
                                    {{--</div>--}}
                                {{--</div>--}}
                                <div class="form-group">
                                    <div class="col-sm-offset-3 col-sm-12">
                                        <button type="submit" class="btn btn-success">
                                            <i class="fa fa-plus"></i> Save
                                        </button>
                                        <a class="btn btn-warning" href="{{ url('/instructors') }}"><i
                                                    class="glyphicon glyphicon-chevron-left"></i> Back</a>
                                    </div>
                                </div>
                            </form>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection

@section('scripts')
    <script>
        window.STARPATH="{{asset('images/raty/')}}";
    </script>
    <script src="{{ asset("vendors/js/extensions/jquery.raty.js")}}"></script>
    <script src="{{ asset("js/scripts/instructors_scripts.js")}}"></script>
@endsection