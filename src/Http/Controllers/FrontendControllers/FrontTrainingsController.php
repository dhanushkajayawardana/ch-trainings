<?php

namespace Creativehandles\ChTrainings\Http\Controllers\FrontendControllers;

use Creativehandles\ChTrainings\Plugins\Trainings\Trainings;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use function view;

class FrontTrainingsController extends Controller
{


    /**
     * @var Trainings
     */
    private $trainings;

    public function __construct(Trainings $trainings)
    {

        $this->trainings = $trainings;
    }

    public function show($id)
    {
        $data = $this->trainings->findTrainingById($id,['linkedFeedback','reccomendedTrainings','feedback']);
        return $data;
    }
}
