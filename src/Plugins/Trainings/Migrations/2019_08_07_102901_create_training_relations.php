<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTrainingRelations extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('training_relations', function (Blueprint $table) {
          $table->increments('id');
          $table->integer('group')->nullable()->unsigned();
          $table->integer('video');
          $table->integer('order');
          $table->timestamps();

          $table->foreign('group')->references('id')->on('training_groups');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('training_relations');
    }
}
