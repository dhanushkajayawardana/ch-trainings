<?php
/**
 * Created by PhpStorm.
 * User: deemantha
 * Date: 16/7/19
 * Time: 10:09 AM
 */

namespace Creativehandles\ChTrainings\Plugins\Trainings\Repositories;


use Creativehandles\ChTrainings\Plugins\Trainings\Models\InstructorModel;
use App\Repositories\BaseEloquentRepository;

class InstructorRepository extends BaseEloquentRepository
{

    public function __construct(InstructorModel $model)
    {
        $this->model = $model;
    }

    public function getAllInstructors()
    {
        return \App\User::where('role_id',\Spatie\Permission\Models\Role::where('name','Instructor')->first()->id)->get();
    }

    public function getInstructorsByCondition($orderColumn=null,$direction="asc",$nameColumn=null,$name=null,$with=[])
    {
        $model = $this->model->with($with)->join('users','users.id','=','instructors.user_id');

        $limit = request()->input('length',10);
        $offset = request()->input('start',0);

        $model = $model->offset($offset)->limit($limit);

        if($nameColumn){
            $model = $model->where($nameColumn,'LIKE','%'.$name.'%')
                ->orWhere('users.first_name','LIKE','%'.$name.'%')
                ->orWhere('users.last_name','LIKE','%'.$name.'%');
        }

        if($orderColumn){
            $model = $model->orderBy($orderColumn,$direction);
        }

        return $model->select(['users.first_name','users.last_name','users.avatar','users.email','users.id as userId','instructors.*'])->get();
    }
}