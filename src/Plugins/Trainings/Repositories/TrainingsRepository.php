<?php
/**
 * Created by PhpStorm.
 * User: deemantha
 * Date: 2/7/19
 * Time: 7:27 PM
 */

namespace Creativehandles\ChTrainings\Plugins\Trainings\Repositories;


use Creativehandles\ChTrainings\Plugins\Trainings\Models\TrainingsModel;
use App\Repositories\BaseEloquentRepository;

class TrainingsRepository extends BaseEloquentRepository
{

    public function __construct(TrainingsModel $model)
    {
        $this->model = $model;
    }

}