<?php
/**
 * Created by PhpStorm.
 * User: deemantha
 * Date: 1/7/19
 * Time: 7:10 PM
 */

namespace Creativehandles\ChTrainings\Plugins\Trainings;


use Creativehandles\ChFeedback\Plugins\Feedback\Repositories\FeedbackRepository;
use Creativehandles\ChLabels\Plugins\Labels\Repositories\LabelRepository;
use Creativehandles\ChTrainings\Plugins\Plugin;
use Creativehandles\ChTrainings\Plugins\Trainings\Repositories\InstructorRepository;
use Creativehandles\ChTrainings\Plugins\Trainings\Repositories\LowTrainingCategoryRepository;
use Creativehandles\ChTrainings\Plugins\Trainings\Repositories\TrainingsRepository;

use Creativehandles\ChTrainings\Plugins\Trainings\Models\Group;
use Creativehandles\ChTrainings\Plugins\Trainings\Models\VideoRelations;
use App\Repositories\CoreRepositories\Contracts\UserRepositoryInterface;
use Illuminate\Support\Facades\Auth;

class Trainings extends Plugin
{

    /**
     * @var TrainingsRepository
     */
    private $trainingsRepository;
    /**
     * @var FeedbackRepository
     */
    private $feedbackRepository;
    /**
     * @var LowTrainingCategoryRepository
     */
    private $categoryRepository;

    /**
     * @var InstructorRepository
     */
    private $instructorRepository;
    /**
     * @var UserRepository
     */
    private $userRepository;
    /**
     * @var LabelRepository
     */
    private $labelRepository;

    public function __construct(
        TrainingsRepository $trainingsRepository,
        FeedbackRepository $feedbackRepository,
        LowTrainingCategoryRepository $categoryRepository,
        InstructorRepository $instructorRepository,
        UserRepositoryInterface $userRepository,
        LabelRepository $labelRepository
    ) {
        parent::__construct();
        $this->trainingsRepository = $trainingsRepository;
        $this->feedbackRepository = $feedbackRepository;
        $this->categoryRepository = $categoryRepository;
        $this->instructorRepository = $instructorRepository;
        $this->userRepository = $userRepository;
        $this->labelRepository = $labelRepository;
    }

    public function getInstructorsByCondition($orderColumn, $direction, $nameColumn, $name, $with)
    {

        return $this->instructorRepository->getInstructorsByCondition($orderColumn, $direction, $nameColumn, $name,
            $with);
    }

    public function getAllInstructors()
    {
        return $this->instructorRepository->getAllInstructors();
    }

    public function createOrUpdateUser($data, $key = 'email')
    {
        return $this->userRepository->updateOrCreate($data, $key);
    }

    public function createOrUpdateInstructor($data)
    {
        return $this->instructorRepository->updateOrCreate($data, 'user_id');
    }

    public function createOrUpdateTraining($data,$id='trainings_id')
    {
        return $this->trainingsRepository->updateOrCreate($data, $id);
    }

    public function getAllTrainings($with)
    {
        return $this->trainingsRepository->allWithPagination(15, $with);
    }

    public function getTrainingByName($name)
    {
        return $this->trainingsRepository->findByLike('trainings_title', '%' . $name . '%');
    }

    public function getTrainingsByVisibility($visibility = true, $with)
    {
        return $this->trainingsRepository->findBy('visibility', (int)$visibility, $with);
    }

    public function findTrainingById($id, $with = [])
    {
        return $this->trainingsRepository->find($id, $with);
    }

    public function currentAuthorsTrainings($trainingID = null)
    {
      if(!$trainingID) {
        return $this->trainingsRepository->all();
      } else {
        return $this->trainingsRepository->all()->except($trainingID);
      }
    }

    public function deleteTrainingById($id)
    {
        return $this->trainingsRepository->deleteById($id);
    }

    public function getNextkey()
    {
        return $this->trainingsRepository->getNextkey('trainings_id');
    }

    /** FEEDBACK */
    public function getAllFeedback()
    {
        return $this->feedbackRepository->all();
    }

    /** Save the groups */
    public function saveGroups($request, $trainingID, $isUpdating = false)
    {

        if ($isUpdating) {
            $this->clearGroupRelations($trainingID);
        }

        if ($request->group) {

            foreach ($request->group as $key => $group) {
                $groupName = (preg_match('/\s/', $group)) ? str_replace(" ", "_", $group) : $group;

                if ($request->has($groupName)) {
                    $groupObject = new Group();

                    $groupObject->training = $trainingID;
                    $groupObject->group = $group;
                    $groupObject->order = $key;

                    $groupObject->save();

                    // the group has a videos
                    foreach ($request->input($groupName) as $videoOrder => $videoID) {

                        $groupRelation = new VideoRelations();

                        $groupRelation->group = $groupObject->id;
                        $groupRelation->video = $videoID;
                        $groupRelation->order = $videoOrder;

                        $groupRelation->save();

                    }
                }

            }

        }

    }

    public function clearGroupRelations($trainingID)
    {
        $groups = Group::where('training', $trainingID)->with('videos')->get();

        if ($groups) {
            foreach ($groups as $group) {
                if ($group->videos) {
                    foreach ($group->videos as $video) {
                        $video->delete();
                    }
                }
                $group->delete();
            }
        }
    }

    /** CATEGORY */
    public function getAllCategories()
    {
        return $this->categoryRepository->all();
    }

    /** TAGS */

    public function createNewTag($data, $condition)
    {
        return $this->labelRepository->updateOrCreateByMultipleKeys($condition, $data);
    }

    public function getAllTags($model = null)
    {
        if ($model) {
            return $this->labelRepository->getBy('model', $model);
        }

        return $this->labelRepository->all();
    }
}