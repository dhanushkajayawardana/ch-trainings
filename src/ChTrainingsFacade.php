<?php

namespace Creativehandles\ChTrainings;

use Illuminate\Support\Facades\Facade;

/**
 * @see \Creativehandles\ChTrainings\Skeleton\SkeletonClass
 */
class ChTrainingsFacade extends Facade
{
    /**
     * Get the registered name of the component.
     *
     * @return string
     */
    protected static function getFacadeAccessor()
    {
        return 'ch-trainings';
    }
}
